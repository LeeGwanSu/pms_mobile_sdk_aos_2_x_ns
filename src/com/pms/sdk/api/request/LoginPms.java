package com.pms.sdk.api.request;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.text.TextUtils;

import com.pms.sdk.api.APIManager.APICallback;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.StringUtil;

public class LoginPms extends BaseRequest {

	public LoginPms(Context context) {
		super(context);
	}

	/**
	 * get param
	 * 
	 * @return
	 */
	public JSONObject getParam (String custId, JSONObject userData) {
		JSONObject jobj;

		try {
			jobj = new JSONObject();
			jobj.put("custId", custId);

			if (userData != null) {
				jobj.put("userData", userData);
			}

			return jobj;
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * request
	 * 
	 * @param custId
	 * @param userData
	 * @param apiCallback
	 */
	public void request (final String custId, final JSONObject userData, final APICallback apiCallback) {
		try
		{
			String lastCustId = mPrefs.getString(PREF_LOGINED_CUST_ID);
			if (!TextUtils.isEmpty(lastCustId))
			{
				if (!custId.equals(mPrefs.getString(PREF_LOGINED_CUST_ID)))
				{
					// 기존의 custId와 auth할려는 custId가 다르다면, DB초기화
					CLog.i("LoginPms:new user");
					mDB.deleteAll();
					mPrefs.putString(PREF_MAX_USER_MSG_ID, "-1");
				}
			}

			JSONObject params = getParam(custId, userData);
			CLog.d("Params: " + params.toString());
			apiManager.call(API_LOGIN_PMS, params, new APICallback()
			{
				@Override
				public void response (String code, JSONObject json)
				{
					if (CODE_SUCCESS.equals(code))
					{
						mPrefs.putString(PREF_LOGINED_CUST_ID, custId);
						requiredResultProc(json);
					}
					if (apiCallback != null)
					{
						apiCallback.response(code, json);
					}
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * required result proccess
	 * 
	 * @param json
	 */
	private boolean requiredResultProc (JSONObject json)
	{
		String notiFlag = null, mktFlag = null;
		try
		{
			notiFlag = json.getString("notiFlag");
			mktFlag = json.getString("mktFlag");
		}
		catch (JSONException e)
		{
			CLog.e(e.getMessage());
		}

		mPrefs.putString(PREF_LOGINED_STATE, (TextUtils.isEmpty(PMSUtil.getCustId(mContext)) ? FLAG_N : FLAG_Y));
		mPrefs.putString(PREF_NOTI_FLAG, (TextUtils.isEmpty(notiFlag) ? FLAG_N : notiFlag));
		mPrefs.putString(PREF_MKT_FLAG, (TextUtils.isEmpty(mktFlag) ? FLAG_N : mktFlag));

		return true;
	}
}
