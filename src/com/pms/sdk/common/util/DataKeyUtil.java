package com.pms.sdk.common.util;

import android.content.Context;
import android.database.Cursor;

import com.pms.sdk.bean.Data;
import com.pms.sdk.db.PMSDB;

public class DataKeyUtil {

	public static void setDBKey (Context con, String key, String value) {
		PMSDB db = PMSDB.getInstance(con);
		int size = db.selectKeyData(key);
		if (size > 0)
		{
			Cursor c = db.selectKey(key);
			c.moveToFirst();
			Data data = new Data(c);
			if (value.equals(data.value) == false)
			{
				db.updateDataValue(key, value);
			}
		}
		else
		{
			CLog.d("new perfs Item. key: " + key + ", value: " + value);

			Data data = new Data();
			data.key = key;
			data.value = value;
			CLog.d("STATUS : " + ((db.insertDataValue(data)) > 0 ? "SUCCESS" : "FAIL"));
		}
	}

	public static String getDBKey (Context con, String key) {
		PMSDB db = PMSDB.getInstance(con);
		Cursor c = db.selectKey(key);
		if (c.getCount() < 1)
		{
			CLog.w("[ " + key + " ] data is empty");
			return "";
		}

		try
		{
			c.moveToFirst();
			Data data = new Data(c);

			return data.value;
		}
		catch (Exception e) { }
		finally
		{
			c.close();
		}

		return "";
	}
}
