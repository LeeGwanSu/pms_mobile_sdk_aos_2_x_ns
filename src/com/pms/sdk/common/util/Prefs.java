package com.pms.sdk.common.util;

import com.pms.sdk.IPMSConsts;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

/**
 * 
 * @author erzisk
 * @since 2013.06.11
 */
public class Prefs implements IPMSConsts {

	private SharedPreferences prefs;
	private Editor edit;

	public Prefs(Context context) {
		prefs = context.getSharedPreferences(PREF_FILE_NAME, Activity.MODE_PRIVATE);
		edit = prefs.edit();
	}

	public void putString (String k, String v) {
		try {
			edit.putString(k, v);
		} catch (Exception e) {
			e.printStackTrace();
		}
		edit.commit();
	}

	public String getString (String k) {

		try {
			return prefs.getString(k, "");
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}

	}

	public void putInt (String k, int v) {

		try {
			edit.putInt(k, v);
		} catch (Exception e) {
			e.printStackTrace();
		}
		edit.commit();
	}

	public int getInt (String k) {

		try {
			return prefs.getInt(k, -1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return -1;

	}

	public int getInt (String k, int def) {

		try {
			return prefs.getInt(k, def);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return -1;

	}

	public void putBoolean (String k, boolean v) {

		try {
			edit.putBoolean(k, v);
		} catch (Exception e) {
			e.printStackTrace();
		}

		edit.commit();
	}

	public Boolean getBoolean (String k) {

		try {
			return prefs.getBoolean(k, false);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
}
