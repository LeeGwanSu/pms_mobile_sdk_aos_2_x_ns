package com.pms.sdk;

import android.content.Context;
import android.os.Bundle;

/**
 * 푸시 수신 리스너
 * 
 * @author kim yong
 * 
 */
public interface OnReceivePushListener {

	/**
	 * onReceive
	 * 
	 * @param context
	 * @param bundle
	 * @return true : notification을 출력. <br>
	 *         false : notification을 출력하지 않음.
	 */
	boolean onReceive (Context context, Bundle bundle);
}
